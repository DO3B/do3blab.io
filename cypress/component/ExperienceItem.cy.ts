import { mount } from "cypress/vue";
import ExperienceItem from "../../src/components/ExperienceItem.vue";
import curriculum from "../../src/assets/json/curriculum.json";

describe("ExperienceItem.cy.ts", () => {
  const unescapedCompanyName = curriculum.experiences[0].company.replace(
    " ",
    ""
  );

  it("displays an experience", () => {
    const wrapper = mount(ExperienceItem, {
      props: {
        experience: curriculum.experiences[0],
      },
    });

    wrapper
      .get(`[data-label="ExperienceCompany${unescapedCompanyName}"]`)
      .contains(curriculum.experiences[0].company);
    wrapper
      .get("ul li")
      .should("have.length", curriculum.experiences[0].tasks.length);
  });
});
