// https://docs.cypress.io/api/introduction/api.html

describe("HomeView", () => {
  context("Mackbook 16", () => {
    beforeEach(() => {
      cy.viewport("macbook-16");
      cy.visit("/");
    });

    it("visits the app root url", () => {
      cy.contains(".title", "Loïc MAYOL");
      cy.get("nav .navbar-burger").should("not.be.visible");
      cy.get("nav .navbar-end").should("be.visible");
    });

    it("changes themes", () => {
      cy.get("button:visible[aria-label='Toggle theme']").click();
      cy.get(".hero").filter(".is-dark").should("exist");
    });

    it("when clicking on 'Mes projets', should redirect to /project", () => {
      cy.get("a.navbar-item:visible[href='/projects']").click({ force: true });
      cy.url().should("include", "/projects");
    });

    it("when clicking on 'A propos de moi', should redirect to /about", () => {
      cy.get("a.navbar-item:visible[href='/about']").click({ force: true });
      cy.url().should("include", "/about");
    });
  });

  context("iPhone XR", () => {
    beforeEach(() => {
      cy.viewport("iphone-xr");
      cy.visit("/");
    });

    it("visits the app root url", () => {
      cy.contains(".title", "Loïc MAYOL");
      cy.get("nav .navbar-burger").should("be.visible");
      cy.get("nav .navbar-end").should("not.be.visible");
    });

    it("when clicking on the navbar burger, shoud be active", () => {
      cy.get("a:visible[aria-label='menu']").click();
      cy.get("nav .navbar-burger").should("have.class", "is-active");
    });

    it("changes themes", () => {
      cy.get("a:visible[aria-label='menu']").click();
      cy.get("button:visible[aria-label='Toggle theme']").click();
      cy.get(".hero").filter(".is-dark").should("exist");
    });

    it("when clicking on 'Mes projets', should redirect to /project", () => {
      cy.get("a:visible[aria-label='menu']").click();
      cy.get("a.navbar-item:visible[href='/projects']").click({ force: true });
      cy.url().should("include", "/projects");
    });

    it("when clicking on 'A propos de moi', should redirect to /about", () => {
      cy.get("a:visible[aria-label='menu']").click();
      cy.get("a.navbar-item:visible[href='/about']").click({ force: true });
      cy.url().should("include", "/about");
    });

    it("when clicking again on the navbar burger, shoud not be active", () => {
      cy.get("a:visible[aria-label='menu']").click();
      cy.get("a:visible[aria-label='menu']").click();
      cy.get("nav .navbar-burger").should("not.have.class", "is-active");
    });
  });
});
