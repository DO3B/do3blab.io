import { createApp } from "vue";
import { createPinia } from "pinia";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faMapMarkerAlt, faMoon, faSun } from "@fortawesome/free-solid-svg-icons";
import { faLinkedin, faGitlab, faGithub } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import moment from "moment";
import "moment/dist/locale/fr";

import App from "./App.vue";
import router from "./router";

moment.locale("fr");

library.add(faMapMarkerAlt, faLinkedin, faGitlab, faGithub, faMoon, faSun);
const app = createApp(App);

app.use(createPinia());
app.use(router);

app.component("VueFontawesome", FontAwesomeIcon);

app.mount("#app");
