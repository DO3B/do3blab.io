import moment from "moment";

export function niceDate(value: string, format: string) {
  if (
    value === null ||
    value === undefined ||
    format === undefined ||
    value === ""
  ) {
    return "Date non renseignée";
  }

  if (format === "from") {
    return moment(value, "MM/DD/YYYY").fromNow();
  }

  return moment(value, "MM/DD/YYYY").format(format);
}

export function calculateDuration(dates: {
  start: string;
  end: string | null;
}) {
  const start = moment(dates.start, "MM/DD/YYYY");
  const end = moment(dates.end, "MM/DD/YYYY");
  const duration = moment.duration(end.diff(start));
  return duration.humanize();
}
