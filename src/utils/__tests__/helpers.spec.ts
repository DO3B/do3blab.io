import { describe, it, expect, vi, beforeEach, test } from "vitest";
import { niceDate, calculateDuration } from "../helpers";

describe("Helpers", () => {
  describe("niceDate", () => {
    beforeEach(() => {
      vi.useFakeTimers();
    });

    it("should return Date non renseignée when value is null", () => {
      expect(niceDate(null, "from")).toBe("Date non renseignée");
    });

    it("should return Date non renseignée when value is undefined", () => {
      expect(niceDate(undefined, "from")).toBe("Date non renseignée");
    });

    it("should return Date non renseignée when format is undefined", () => {
      expect(niceDate("2020-01-01", undefined)).toBe("Date non renseignée");
    });

    it("should return Date non renseignée when value is empty", () => {
      expect(niceDate("", "from")).toBe("Date non renseignée");
    });

    it("should return from now when format is from", () => {
      vi.setSystemTime(new Date("02/01/2020"));
      expect(niceDate("01/01/2020", "from")).toBe("a month ago");
    });

    it("should return the date when format is not from", () => {
      expect(niceDate("01/01/2020", "MM/DD/YYYY")).toBe("01/01/2020");
    });
  });

  describe("calculateDuration", () => {
    it("should return Invalid date when start is null", () => {
      expect(calculateDuration({ start: null, end: null })).toBe(
        "Invalid date"
      );
    });

    it("should return Invalid date when end is null", () => {
      expect(calculateDuration({ start: null, end: null })).toBe(
        "Invalid date"
      );
    });

    it("should return Invalid date when start is undefined", () => {
      expect(calculateDuration({ start: undefined, end: undefined })).toBe(
        "Invalid date"
      );
    });

    it("should return Invalid date when end is undefined", () => {
      expect(calculateDuration({ start: undefined, end: undefined })).toBe(
        "Invalid date"
      );
    });

    it("should return Invalid date when start is empty", () => {
      expect(calculateDuration({ start: "", end: "" })).toBe("Invalid date");
    });

    it("should return Invalid date when end is empty", () => {
      expect(calculateDuration({ start: "", end: "" })).toBe("Invalid date");
    });

    it("should return Invalid date when start is not a date", () => {
      expect(calculateDuration({ start: "test", end: "test" })).toBe(
        "Invalid date"
      );
    });

    it("should return Invalid date when end is not a date", () => {
      expect(calculateDuration({ start: "test", end: "test" })).toBe(
        "Invalid date"
      );
    });

    test("should return a year when end is before start", () => {
      expect(
        calculateDuration({ start: "01/01/2020", end: "01/01/2019" })
      ).toBe("a year");
    });
  });
});
