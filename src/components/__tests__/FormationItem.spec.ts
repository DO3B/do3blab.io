import { describe, it, expect } from "vitest";

import { mount } from "@vue/test-utils";
import FormationItem from "../FormationItem.vue";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(fas, fab);

describe("FormationItem", () => {
  it("renders properly", () => {
    const wrapper = mount(FormationItem, {
      props: {
        formation: {
          school: "Une école",
          location: "Marseille, France",
          dates: {
            start: 2017,
            end: 2020,
          },
          diploma: "Un diplôme",
          option: "Une option",
          description: "Une description",
        },
      },
      global: {
        components: {
          VueFontawesome: FontAwesomeIcon,
        },
      },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });
});
