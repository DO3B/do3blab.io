import { describe, test, it, expect, beforeEach } from "vitest";
import { mount } from "@vue/test-utils";
import ExperienceItem from "../ExperienceItem.vue";
import { Experience } from "@/types";
import { niceDate } from "@/utils/helpers";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(fas, fab);

describe("ExperienceItem", () => {
  let classicalExperience: Experience;

  beforeEach(() => {
    classicalExperience = {
      id: "test",
      position: "Travail",
      company: "Entreprise",
      dates: {
        start: "02/03/2020",
        end: null,
      },
      location: "Marseille, France",
      tasks: ["Une tâche", "Une tâche 2"],
    };
  });
  it("renders properly", () => {
    const wrapper = mount(ExperienceItem, {
      props: {
        experience: classicalExperience,
      },
      global: {
        components: {
          VueFontawesome: FontAwesomeIcon,
        },
      },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });

  test("should display Actuellement when end is null", () => {
    const wrapper = mount(ExperienceItem, {
      props: {
        experience: classicalExperience,
      },
      global: {
        components: {
          VueFontawesome: FontAwesomeIcon,
        },
      },
    });

    expect(wrapper.text()).toContain("Actuellement");
  });

  test("should only display the month and the year when end is not null", () => {
    classicalExperience.dates.end = "01/27/2022";

    const wrapper = mount(ExperienceItem, {
      props: {
        experience: classicalExperience,
      },
      global: {
        components: {
          VueFontawesome: FontAwesomeIcon,
        },
      },
    });

    expect(wrapper.text()).toContain(
      niceDate(classicalExperience.dates.end, "MMMM YYYY")
    );
  });
});
