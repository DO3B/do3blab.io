import { describe, it, expect } from "vitest";

import { shallowMount } from "@vue/test-utils";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import ProjectItem from "../ProjectItem.vue";

library.add(fas, fab);

describe("ProjectItem", () => {
  it("renders properly", () => {
    const wrapper = shallowMount(ProjectItem, {
      props: {
        project: {
          id: "string",
          webUrl: "string",
          visibility: "string",
          name: "string",
          description: "string",
          avatarUrl: "string",
          topics: ["string"],
        },
      },
      global: {
        components: {
          VueFontawesome: FontAwesomeIcon,
        },
      },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });
});
