import { Project } from "@/types";
import axios from "axios";
import { defineStore } from "pinia";

interface State {
  allProjects: Project[];
}

export const useProjectsStore = defineStore("projects", {
  state: (): State => ({
    allProjects: [],
  }),
  getters: {
    projects: (state: State) => state.allProjects,
  },
  actions: {
    getAllProjects() {
      axios({
        url: "https://gitlab.com/api/graphql",
        method: "POST",
        data: {
          query: `
          {
            do3b: namespace(fullPath: "DO3B") {
              projects {
                edges {
                  node {
                    id
                    visibility
                    webUrl
                    name
                    description
                    avatarUrl
                    topics
                  }
                }
              }
            }
            ceasy: namespace(fullPath: "c-easy") {
              projects {
                edges {
                  node {
                    id
                    visibility
                    webUrl
                    name
                    description
                    avatarUrl
                    topics
                  }
                }
              }
            }
          }
                `,
        },
        headers: {
          // eslint-disable-next-line
          'Authorization': 'Bearer ' + import.meta.env.VITE_GITLAB_API_TOKEN
        },
      })
        .then((response) => {
          const result: Project[] = [];
          response.data.data.do3b.projects.edges.forEach((element: any) => {
            if (element.node.visibility == "public") {
              result.push(element.node);
            }
          });
          response.data.data.ceasy.projects.edges.forEach((element: any) => {
            if (element.node.visibility == "public") {
              result.push(element.node);
            }
          });
          this.allProjects = result;
        })
        .catch((response) => {
          console.log(response);
          console.log(response.data);
        });
    },
  },
});
