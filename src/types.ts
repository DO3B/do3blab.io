export interface Experience {
  id: string;
  position: string;
  company: string;
  dates: {
    start: string;
    end: string | null;
  };
  location: string;
  tasks: string[];
}

export interface Formation {
  id: string;
  school: string;
  location: string;
  dates: {
    start: number;
    end: number;
  };
  diploma: string;
  option?: string;
  description: string;
}

export interface Project {
  id: string;
  webUrl: string;
  visibility: string;
  name: string;
  description: string;
  avatarUrl: string;
  topics: string[];
}
